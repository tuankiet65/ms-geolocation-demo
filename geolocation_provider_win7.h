#pragma once

#include "geolocation_provider.h"

#include <wrl.h>
#include <locationapi.h>

#pragma warning (disable: 4995)

using namespace Microsoft::WRL;

class GeolocationProviderWin7 : public GeolocationProvider {
public:
    GeolocationProviderWin7();
    ~GeolocationProviderWin7();

    bool Initialize() override;
    bool RequestPermission() override;
    bool GetLocation(double *latitude, double *longitude) override;

    bool initialized() const override { return initialized_; }

private:
    ComPtr<ILocation> ilocation_;

    bool initialized_;
};
