#pragma once

#include "geolocation_provider.h"

#include <wrl.h>
#include <locationapi.h>
#include <windows.devices.geolocation.h>

using namespace Microsoft::WRL;
using namespace Microsoft::WRL::Wrappers;
using namespace ABI::Windows::Devices::Geolocation;

class GeolocationProviderUWP : public GeolocationProvider {
public:
    GeolocationProviderUWP(bool require_request_permission);

    bool Initialize() override;
    bool RequestPermission() override;
    bool GetLocation(double *latitude, double *longitude) override;

    bool initialized() const override { return initialized_; }

private:
    ComPtr<IGeolocator> igeolocator_;
    RoInitializeWrapper ro_scoped_initializer_;

    bool initialized_;
    bool require_request_permission_;
};
