// Heavily based on 
// https://github.com/qt/qtlocation/blob/5.12/src/plugins/position/winrt/qgeopositioninfosource_winrt.cpp

#include "geolocation_provider_uwp.h"

#include <cassert>
#include <iostream>

#include <Windows.h>
#include <Windows.Foundation.h>
#include <roapi.h>
#include <hstring.h>
#include <wrl/wrappers/corewrappers.h>
#include <wrl/client.h>
#include <wrl/implements.h>
#include <windows.devices.geolocation.h>

using namespace Microsoft::WRL;
using namespace ABI::Windows::Devices::Geolocation;
using namespace ABI::Windows::Foundation;

namespace {

template <class T>
bool WaitForAsyncTaskToComplete(ComPtr<IAsyncOperation<T>> &async_op) {
    ComPtr<IAsyncInfo> async_info;
    HRESULT hr = async_op.As<IAsyncInfo>(&async_info);
    if (FAILED(hr)) {
        std::cerr << "Unable to obtain IAsyncInfo from IAsyncOperation: 0x" << std::hex << hr << std::endl;
        return false;
    }

    std::cerr << "Waiting for async request to complete..." << std::endl;
    while (true) {
        AsyncStatus task_status;
        hr = async_info->get_Status(&task_status);
        if (FAILED(hr)) {
            std::cerr << "Unable to obtain task status: 0x" << std::hex << hr << std::endl;
            return false;
        }

        if (task_status != AsyncStatus::Started)
            break;

        Sleep(1);
    }

    AsyncStatus task_status;
    hr = async_info->get_Status(&task_status);
    if (FAILED(hr)) {
        std::cerr << "Unable to obtain task status: 0x" << std::hex << hr << std::endl;
        return false;
    }

    switch (task_status) {
    case AsyncStatus::Error:
        std::cerr << "Error while performing async task" << std::endl;
        return false;
    case AsyncStatus::Canceled:
        std::cerr << "Async task cancelled" << std::endl;
        return false;
    case AsyncStatus::Completed:
        std::cerr << "Async task completed successfully" << std::endl;
        return true;
    }

    assert(false);
    return false;
}

}  // namespace

GeolocationProviderUWP::GeolocationProviderUWP(bool require_request_permission)
    : initialized_(false), require_request_permission_(require_request_permission),
      ro_scoped_initializer_(RO_INIT_MULTITHREADED) {
    Initialize();
}

bool GeolocationProviderUWP::Initialize() {
    if (initialized_)
        return true;

    HRESULT hr = static_cast<HRESULT>(ro_scoped_initializer_);
    if (FAILED(hr)) {
        std::cerr << "Unable to initialize Windows Runtime: 0x" << std::hex << hr << std::endl;
        return false;
    }

    hr = ::RoActivateInstance(
        Wrappers::HString::MakeReference(RuntimeClass_Windows_Devices_Geolocation_Geolocator).Get(),
        &igeolocator_);
    if (FAILED(hr)) {
        std::cerr << "Unable to activate an instance of ABI::Windows::Devices::Geolocation::Geolocator: 0x" << std::hex << hr << std::endl;
        return false;
    }

    initialized_ = true;
    return true;
}

bool GeolocationProviderUWP::RequestPermission() {
    if (!initialized_) {
        std::cerr << "Not initialized" << std::endl;
        return false;
    }

    if (!require_request_permission_)
        return true;

    ComPtr<IGeolocatorStatics> igeolocatorstatics;
    HRESULT hr = ::RoGetActivationFactory(
        Wrappers::HString::MakeReference(RuntimeClass_Windows_Devices_Geolocation_Geolocator).Get(),
        IID_PPV_ARGS(&igeolocatorstatics));
    if (FAILED(hr)) {
        std::cerr << "Unable to get IGeolocatorStatics: 0x" << std::hex << hr << std::endl;
        return false;
    }

    ComPtr<IAsyncOperation<GeolocationAccessStatus>> async_op;
    hr = igeolocatorstatics->RequestAccessAsync(&async_op);
    if (FAILED(hr)) {
        std::cerr << "Unable to request access: 0x" << std::hex << hr << std::endl;
        return false;
    }

    if (!WaitForAsyncTaskToComplete<GeolocationAccessStatus>(async_op)) {
        std::cerr << "Async task to request permission failed" << std::endl;
        return false;
    }

    GeolocationAccessStatus access_status;
    hr = async_op->GetResults(&access_status);
    if (FAILED(hr)) {
        std::cerr << "Unable to fetch async result" << std::endl;
        return false;
    }

    switch (access_status) {
    case GeolocationAccessStatus_Denied:
        std::cerr << "User explicitly denied geolocation access" << std::endl;
        return false;
    case GeolocationAccessStatus_Unspecified:
        std::cerr << "Unspecified access status" << std::endl;
        return false;
    case GeolocationAccessStatus_Allowed:
        std::cerr << "User explicitly allowed geolocation access" << std::endl;
        return true;
    }

    assert(false);
    return false;
}

bool GeolocationProviderUWP::GetLocation(double *latitude, double *longitude) {
    if (!initialized_) {
        std::cerr << "Not initialized" << std::endl;
        return false;
    }

    ComPtr<IAsyncOperation<Geoposition*>> geoposition_async_op;
    HRESULT hr = igeolocator_->GetGeopositionAsync(&geoposition_async_op);
    if (FAILED(hr)) {
        std::cerr << "Unable to request geoposition: 0x" << std::hex << hr << std::endl;
        return false;
    }

    if (!WaitForAsyncTaskToComplete<Geoposition*>(geoposition_async_op)) {
        std::cerr << "Async task to get location failed" << std::endl;
        return false;
    }

    ComPtr<IGeoposition> position;
    hr = geoposition_async_op->GetResults(&position);
    if (FAILED(hr)) {
        std::cerr << "Unable to fetch Geoposition from async job: 0x" << std::hex << hr << std::endl;
        return false;
    }

    ComPtr<IGeocoordinate> coordinate;
    hr = position->get_Coordinate(&coordinate);
    if (FAILED(hr)) {
        std::cerr << "Unable to fetch Coordinate from Position: 0x" << std::hex << hr << std::endl;
        return false;
    }

    ComPtr<IGeocoordinateWithPoint> coordinate_with_point;
    hr = coordinate.As<IGeocoordinateWithPoint>(&coordinate_with_point);
    if (FAILED(hr)) {
        std::cerr << "Unable to convert IGeocoordinate to IGeocoordinateWithPoint: 0x" << std::hex << hr << std::endl;
        return false;
    }

    ComPtr<IGeopoint> geopoint;
    hr = coordinate_with_point->get_Point(&geopoint);
    if (FAILED(hr)) {
        std::cerr << "Unable to fetch Point from coordinate: 0x" << std::hex << hr << std::endl;
        return false;
    }

    BasicGeoposition basic_position;
    hr = geopoint->get_Position(&basic_position);
    if (FAILED(hr)) {
        std::cerr << "Unable to fetch Position from geopoint: 0x" << std::hex << hr << std::endl;
        return false;
    }

    *latitude = basic_position.Latitude;
    *longitude = basic_position.Longitude;

    return true;
}
