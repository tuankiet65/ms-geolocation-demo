#include <cassert>
#include <iostream>
#include <memory>

#include <Windows.h>
#include <VersionHelpers.h>

#include "geolocation_provider.h"
#include "geolocation_provider_win7.h"
#include "geolocation_provider_uwp.h"

enum WindowsVersion {
    WINDOWS_7 = 1,
    WINDOWS_8 = 2,
    WINDOWS_10 = 3
};

WindowsVersion SelectWindowsVersion() {
    std::cout << "Select Windows version: " << std::endl;
    std::cout << "1. Windows 7" << std::endl;
    std::cout << "2. Windows 8" << std::endl;
    std::cout << "3. Windows 10" << std::endl;
    std::cout << std::endl;

    int choice;
    while (true) {
        std::cout << "Your choice: ";
        std::cin >> choice;

        if ((1 <= choice) && (choice <= 3)) break;
    }

    return static_cast<WindowsVersion>(choice);
}

int main() {
    if (!IsWindows7OrGreater()) {
        std::cerr << "This demo only runs on Windows 7 and newer" << std::endl;
        return 1;
    }

    std::unique_ptr<GeolocationProvider> geolocation_provider;
    switch (SelectWindowsVersion()) {
    case WINDOWS_7:
        geolocation_provider = std::make_unique<GeolocationProviderWin7>();
        break;
    case WINDOWS_8:
        geolocation_provider = std::make_unique<GeolocationProviderUWP>(/* require_request_permission */ false);
        break;
    case WINDOWS_10:
        geolocation_provider = std::make_unique<GeolocationProviderUWP>(/* require_request_permission */ true);
        break;
    }

    assert(geolocation_provider->initialized());
    while (true) {
        if (!geolocation_provider->RequestPermission()) {
            std::cerr << "Unable to request permission" << std::endl;
        }

        double latitude, longitude;
        if (!geolocation_provider->GetLocation(&latitude, &longitude)) {
            std::cerr << "Unable to get location" << std::endl;
        } else {
            std::cout << "lat: " << latitude << ", long = " << longitude << std::endl;
        }

        Sleep(1000);
    }

    return 0;
}
