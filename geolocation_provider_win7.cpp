#include "geolocation_provider_win7.h"

#include <iostream>

#include <wrl.h>
#include <locationapi.h>

using namespace Microsoft::WRL;

GeolocationProviderWin7::GeolocationProviderWin7()
    : initialized_(false) {
    Initialize();
}

bool GeolocationProviderWin7::Initialize() {
    if (initialized_)
        return true;

    HRESULT hr = ::CoInitializeEx(nullptr, COINIT_MULTITHREADED);
    if (FAILED(hr)) {
        std::cerr << "Unable to initialize COM: 0x" << std::hex << hr << std::endl;
        return false;
    }

    hr = ::CoCreateInstance(CLSID_Location, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&ilocation_));
    if (FAILED(hr)) {
        std::cerr << "Unable to create COM instance to ILocation: 0x" << std::hex << hr << std::endl;
        return false;
    }

    initialized_ = true;
    return true;
}

GeolocationProviderWin7::~GeolocationProviderWin7() {
    ::CoUninitialize();
}

bool GeolocationProviderWin7::RequestPermission() {
    if (!initialized_) {
        std::cerr << "Not initialized" << std::endl;
        return false;
    }

    IID report_types[] = {
        IID_ILatLongReport
    };
    HRESULT hr = ilocation_->RequestPermissions(nullptr, report_types, ARRAYSIZE(report_types), TRUE);
    if (FAILED(hr)) {
        std::cerr << "Unable to request location permission: 0x" << std::hex << hr;
        switch (hr) {
        case HRESULT_FROM_WIN32(ERROR_ACCESS_DENIED):
            std::cerr << ", ERROR_ACCESS_DENIED" << std::endl;
            return false;
        case HRESULT_FROM_WIN32(ERROR_CANCELLED):
            std::cerr << ", ERROR_CANCELLED" << std::endl;
            return false;
        default:
            std::cerr << ", (unknown error)" << std::endl;
            return false;
        }
    }

    return true;
}

bool GeolocationProviderWin7::GetLocation(double *latitude, double *longitude) {
    if (!initialized_) {
        std::cerr << "Not initialized" << std::endl;
        return false;
    }

    LOCATION_REPORT_STATUS status;
    HRESULT hr = ilocation_->GetReportStatus(IID_ILatLongReport, &status);
    if (FAILED(hr)) {
        std::cerr << "Unable to fetch report status: 0x" << std::hex << hr << std::endl;
        return false;
    }

    std::cerr << "Report status: ";
    switch (status) {
    case REPORT_NOT_SUPPORTED:
        std::cerr << "REPORT_NOT_SUPPORTED" << std::endl;
        return false;
    case REPORT_ERROR:
        std::cerr << "REPORT_ERROR" << std::endl;
        return false;
    case REPORT_ACCESS_DENIED:
        std::cerr << "REPORT_ACCESS_DENIED" << std::endl;
        return false;
    case REPORT_INITIALIZING:
        std::cerr << "REPORT_INITIALIZING" << std::endl;
        return false;
    case REPORT_RUNNING:
        std::cerr << "REPORT_RUNNING" << std::endl;
        return false;
    }

    ComPtr<ILocationReport> location_report;
    hr = ilocation_->GetReport(IID_ILatLongReport, &location_report);
    if (FAILED(hr)) {
        std::cerr << "Unable to fetch location report: 0x" << std::hex << hr << std::endl;
        return false;
    }

    ComPtr<ILatLongReport> latlong_report;
    hr = location_report->QueryInterface(IID_ILatLongReport, &latlong_report);
    if (FAILED(hr)) {
        std::cerr << "Unable to convert ILocationReport to ILatLongReport: 0x" << std::hex << hr << std::endl;
        return false;
    }

    hr = latlong_report->GetLatitude(latitude);
    if (FAILED(hr)) {
        std::cerr << "Unable to fetch Latitude from ILatLongReport: 0x" << std::hex << hr << std::endl;
        return false;
    }

    hr = latlong_report->GetLongitude(longitude);
    if (FAILED(hr)) {
        std::cerr << "Unable to fetch longitude from ILatLongReport: 0x" << std::hex << hr << std::endl;
        return false;
    }

    return true;
}
