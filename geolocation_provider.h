#pragma once

class GeolocationProvider {
public:
    virtual bool Initialize() = 0;
    virtual bool RequestPermission() = 0;
    virtual bool GetLocation(double *latitude, double *longitude) = 0;

    virtual bool initialized() const = 0;
};
